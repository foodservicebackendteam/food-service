package org.therapfoods.entity;

import javax.persistence.Embeddable;

/**
 * Created by niaz on 11/23/14.
 */

@Embeddable
public class User
{
    private String user_id;

    private String name;



    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }





}
