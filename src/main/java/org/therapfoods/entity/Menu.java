package org.therapfoods.entity;


import javax.persistence.*;
import java.util.*;

@Entity
public class Menu {

    @Id
    @GeneratedValue
    @Column
    Integer id;

    @Column
    @Temporal(TemporalType.DATE)
    Date date;

    @ElementCollection
    List<String> foods= new ArrayList<String>();

    public Menu(){
        date= new Date();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public List<String> getFoods() {
        return foods;
    }

    public void setFoods(List<String> foods) {
        this.foods = foods;
    }

    @Override
    public String toString() {
        return "Menu{" +
                "id=" + id +
                ", date=" + date +
                ", foods=" + foods +
                '}';
    }
}
