package org.therapfoods.entity;


import org.therapfoods.utils.MealConfirmationStatus;
import org.therapfoods.utils.MealConsumptionStatus;

import javax.persistence.*;
import java.util.Date;

@Entity
public class FoodRegistry {

    @Id
    @GeneratedValue
    @Column
    Integer id;

    @Column
    Date date;


    @Embedded
    private User user;

    @Column
    MealConfirmationStatus mealConfirmationStatus;

    @Column
    MealConsumptionStatus mealConsumptionStatus;


    public MealConfirmationStatus getMealConfirmationStatus() {
        return mealConfirmationStatus;
    }

    public void setMealConfirmationStatus(MealConfirmationStatus mealConfirmationStatus) {
        this.mealConfirmationStatus = mealConfirmationStatus;
    }




    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public MealConsumptionStatus getMealConsumptionStatus() {
        return mealConsumptionStatus;
    }

    public void setMealConsumptionStatus(MealConsumptionStatus mealConsumptionStatus) {
        this.mealConsumptionStatus = mealConsumptionStatus;
    }



    public FoodRegistry()
    {
        date= new Date();
        mealConfirmationStatus = MealConfirmationStatus.NOT_CONFIRMED;
        mealConsumptionStatus=MealConsumptionStatus.NOT_CONSUMED;
    }



    @Override
    public String toString() {
        return "FoodRegistry{" +
                "id=" + id +
                ", date=" + date +
                ", user=" + user +
                ", mealConsumptionStatus=" + mealConsumptionStatus +
                '}';
    }
}
