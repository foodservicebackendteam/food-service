package org.therapfoods.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.*;


@Entity
public class EmployeeCount
{
	@Id
	@Temporal(TemporalType.DATE)
	@Column
	private Date date;

    @Column
    private int totalEmployee;

    /*
    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable( name="Menu",
            joinColumns={@JoinColumn(name="date_id")},
            inverseJoinColumns={@JoinColumn(name="food_id")})
    private List<FoodItem> foodItems = new ArrayList<FoodItem> ();

    public List<FoodItem> getFoodItems() {
		return foodItems;
	}

	public void setFoodItems(List<FoodItem> foodItems) {
		this.foodItems = foodItems;
	}
    */


    public int getTotalEmployee()
    {
        return totalEmployee;
    }

    public void setTotalEmployee(int totalEmployee) {
        this.totalEmployee = totalEmployee;
    }

	

	public EmployeeCount()
	{
		this.date = new Date();
	}
	

	public Date getDate() 
	{
		return date;
	}

	public void setDate(Date date) 
	{
		this.date = date;
	}

}
