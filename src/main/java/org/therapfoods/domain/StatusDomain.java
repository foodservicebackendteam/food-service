package org.therapfoods.domain;

import org.therapfoods.utils.MealConfirmationStatus;
import org.therapfoods.utils.MealConsumptionStatus;

/**
 * Created by niaz on 11/23/14.
 */
public class StatusDomain
{
    private String status;

    public String getStatus()
    {
        return status;
    }

    public void setStatus(MealConfirmationStatus mealConfirmationStatus)
    {
        if (mealConfirmationStatus == MealConfirmationStatus.CONFIRMED)
        {
            this.status = "confirmed";
        }
        else
        {
            this.status = "not_confirmed";
        }
    }

    public void setStatus(MealConsumptionStatus mealConsumptionStatus)
    {
        if (mealConsumptionStatus == MealConsumptionStatus.CONSUMED)
        {
            this.status = "consumed";
        }
        else
        {
            this.status = "not_consumed";
        }
    }
}
