package org.therapfoods.domain;

import org.therapfoods.utils.MealConfirmationStatus;
import org.therapfoods.utils.MealConsumptionStatus;

import javax.persistence.Column;

/**
 * Created by niaz on 11/23/14.
 */

public class UserDomain
{
    private String user_id;
    private String name;
    private MealConfirmationStatus mealConfirmationStatus;
    private MealConsumptionStatus mealConsumptionStatus;

    public MealConfirmationStatus getMealConfirmationStatus() {
        return mealConfirmationStatus;
    }

    public void setMealConfirmationStatus(MealConfirmationStatus mealConfirmationStatus) {
        this.mealConfirmationStatus = mealConfirmationStatus;
    }

    public MealConsumptionStatus getMealConsumptionStatus() {
        return mealConsumptionStatus;
    }

    public void setMealConsumptionStatus(MealConsumptionStatus mealConsumptionStatus) {
        this.mealConsumptionStatus = mealConsumptionStatus;
    }



    public String getUser_id()
    {
        return user_id;
    }

    public void setUser_id(String user_id)
    {
        this.user_id = user_id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }
}
