package org.therapfoods.domain;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by niaz on 11/23/14.
 */

public class MenuDomain
{
    @Temporal(TemporalType.DATE)
    private Date date;

    private List<String> foods= new ArrayList<String>();

    public Date getDate()
    {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public List<String> getFoods() {
        return foods;
    }

    public void setFoods(List<String> foods)
    {
        this.foods = foods;
    }
}
