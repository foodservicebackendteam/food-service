package org.therapfoods.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


/**
 * Created by minhazul.haq on 11/16/14.
 */

@RestController
@RequestMapping("/confirmations")
public class ConfirmationController
{
    /*
    @Autowired
    private ConfirmationDao confirmationDao;
    */

    /*
    caller: TFM Gatekeeper
    purpose: GET list of registered users
    */
    @RequestMapping( method= RequestMethod.GET )
    public void getRegisteredUsers()
    {

    }


    /*
    caller: TFM Gatekeeper
    purpose: Check if user is registered or not
    */
    @RequestMapping( value="/userId" , method= RequestMethod.GET )
    public boolean checkRegistration()
    {
        return true;
    }


    /*
    caller: TFM Food Registration
    purpose: Register a user
    */
    @RequestMapping( method= RequestMethod.POST )
    public void register()
    {

    }


    /*
    caller: TFM Food Registration
    purpose: Register a list of users
    */
    @RequestMapping( method= RequestMethod.PUT )
    public void registerAll()
    {

    }


    /*
    caller: TFM Food Registration
    purpose: Delete a previously registered user
    */
    @RequestMapping( method= RequestMethod.DELETE )
    public void deleteRegistration()
    {

    }


    public void abc()
    {


    }


}
