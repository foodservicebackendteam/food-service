package org.therapfoods.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.therapfoods.domain.MenuDomain;
import org.therapfoods.entity.Menu;
import org.therapfoods.service.MenuService;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by minhazul.haq on 11/16/14.
 */

@RestController
@RequestMapping("/menus")
public class MenuController
{
    @Autowired
    private MenuService menuService;


    @RequestMapping( method=RequestMethod.POST )
    public void postMenu()
    {
        //error
    }


    @RequestMapping( method=RequestMethod.GET )
    public MenuDomain getMenu(HttpServletResponse response)
    {
        response.setHeader("Access-Control-Allow-Origin", "*");
        response.setHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE");
        response.setHeader("Access-Control-Max-Age", "3600");
        response.setHeader("Access-Control-Allow-Headers", "x-requested-with");

        menuService.generateData();

        return menuService.getMenu();
    }


    @RequestMapping( method=RequestMethod.PUT )
    public void putMenu(MenuDomain menuDomain)
    {
        menuService.setMenu(menuDomain);

    }


    @RequestMapping( method=RequestMethod.DELETE )
    public void deleteMenu()
    {
        menuService.deleteMenu();
    }

}
