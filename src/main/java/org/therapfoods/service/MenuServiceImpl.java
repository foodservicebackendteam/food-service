package org.therapfoods.service;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.therapfoods.domain.MenuDomain;
import org.therapfoods.entity.Menu;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by minhazul.haq on 11/21/2014.
 */

public class MenuServiceImpl implements MenuService
{
	@Autowired
    private SessionFactory sessionFactory;


    @Override
    @Transactional
    public void generateData() {
        /*
        EmployeeCount employeeCount = new EmployeeCount();
        employeeCount.setTotalEmployee(100);

        session.save(employeeCount);


        User user = new User();
        user.setUser_id("1234");
        user.setName("Niaz");

        FoodRegistry foodRegistry = new FoodRegistry();
        foodRegistry.setUser(user);

        session.save(foodRegistry);
        */
        Session session = sessionFactory.getCurrentSession();


        Criteria cr = session.createCriteria(Menu.class);
        cr.add(Restrictions.eq("date", new Date()));

        if (cr.list().isEmpty())
        {
            Menu menu = new Menu();
            List<String> foodList = new ArrayList<String>();
            foodList.add("Rice");
            foodList.add("Beef");
            foodList.add("Dal");
            menu.setFoods(foodList);

            session.save(menu);
        }
    }


    @Override
    @Transactional
    public boolean setMenu(MenuDomain menuDomain)
    {
    	Session session = sessionFactory.getCurrentSession();


        Criteria cr = session.createCriteria(Menu.class);
        cr.add(Restrictions.eq("date", new Date()));

        Menu menu;

        if (cr.list().isEmpty())
        {
            menu = new Menu();
            menu.setFoods(menuDomain.getFoods());
            session.save(menu);
        }
        else
        {
            menu = (Menu) cr.list().get(0);
            menu.setFoods(menuDomain.getFoods());
            session.update(menu);
        }

        return true;
    }

    @Override
    public MenuDomain getMenu()
    {
        Session session = sessionFactory.openSession();

        String hqlQuery = "FROM Menu WHERE date = :today";
        Query query = session.createQuery(hqlQuery);

        Date today = new Date();
        query.setParameter("today",today);

        List results = query.list();

        if (!results.isEmpty())
        {
            Menu menu = (Menu) results.get(0);

            MenuDomain menuDomain = new MenuDomain();
            menuDomain.setDate(menu.getDate());
            menuDomain.setFoods(menu.getFoods());

            return menuDomain;
        }
        else
        {
            return null;
        }
    }

    @Override
    @Transactional
    public boolean deleteMenu() 
    {
    	Session session = sessionFactory.getCurrentSession();

        Criteria cr = session.createCriteria(Menu.class);
        cr.add(Restrictions.eq( "date", new Date() ));
        Menu menu = (Menu) cr.list().get(0);

        menu.getFoods().clear();
    	session.update(menu);  	

        return true;
    }

}
