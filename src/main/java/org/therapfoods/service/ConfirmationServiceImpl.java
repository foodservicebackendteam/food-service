package org.therapfoods.service;

import org.therapfoods.domain.StatusDomain;
import org.therapfoods.domain.UserDomain;
import org.therapfoods.entity.User;
import org.therapfoods.utils.MealConfirmationStatus;
import org.therapfoods.utils.MealConsumptionStatus;

import java.util.List;

/**
 * Created by minhazul.haq on 11/21/2014.
 */

public class ConfirmationServiceImpl implements ConfirmationService {
    @Override
    public List<UserDomain> getAllConfirmedUsers() {
        return null;
    }

    @Override
    public boolean deleteAllConfirmedUsers() {
        return false;
    }

    @Override
    public boolean confirmUser(UserDomain userDomain) {
        return false;
    }

    @Override
    public StatusDomain getConfirmationStatus(UserDomain userDomain) {
        return null;
    }

    @Override
    public boolean deleteConfirmation(UserDomain userDomain) {
        return false;
    }
}
