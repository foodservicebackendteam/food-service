package org.therapfoods.service;

import org.therapfoods.domain.StatusDomain;
import org.therapfoods.domain.UserDomain;
import org.therapfoods.utils.MealConfirmationStatus;

import java.util.List;

/**
 * Created by niaz on 11/23/14.
 */
public interface ConsumptionService
{
    public List<UserDomain> getAllConsumedUsers();
    public boolean setConsumptionStatus(UserDomain userDomain);
    public StatusDomain getConsumptionStatus(UserDomain userDomain);
}
