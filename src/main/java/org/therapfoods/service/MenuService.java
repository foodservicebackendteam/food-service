package org.therapfoods.service;


import org.therapfoods.domain.MenuDomain;
import org.therapfoods.entity.Menu;


public interface MenuService
{
    public boolean setMenu(MenuDomain menuDomain);
    public MenuDomain getMenu();
    public boolean deleteMenu();


    public void generateData();
}
