package org.therapfoods.service;


import org.therapfoods.domain.StatusDomain;
import org.therapfoods.domain.UserDomain;

import java.util.List;


public interface ConfirmationService
{
    public List<UserDomain> getAllConfirmedUsers();
    public boolean deleteAllConfirmedUsers();
    public boolean confirmUser(UserDomain userDomain);
    public StatusDomain getConfirmationStatus(UserDomain userDomain);
    public boolean deleteConfirmation(UserDomain userDomain);
}
