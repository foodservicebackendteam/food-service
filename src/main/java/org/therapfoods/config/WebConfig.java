package org.therapfoods.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

/**
 * Created by minhazul.haq on 11/16/14.
 */

@Configuration
@EnableWebMvc
@ComponentScan("org.therapfoods.controller")
public class WebConfig
{

}
