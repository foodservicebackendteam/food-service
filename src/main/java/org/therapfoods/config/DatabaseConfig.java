package org.therapfoods.config;

        import org.hibernate.SessionFactory;
        import org.springframework.context.annotation.Bean;
        import org.springframework.context.annotation.Configuration;
        import org.springframework.context.annotation.EnableAspectJAutoProxy;
        import org.springframework.jdbc.datasource.DriverManagerDataSource;
        import org.springframework.orm.hibernate4.HibernateTransactionManager;
        import org.springframework.orm.hibernate4.LocalSessionFactoryBean;
        import org.springframework.transaction.PlatformTransactionManager;
        import org.springframework.transaction.annotation.EnableTransactionManagement;
        import org.springframework.web.servlet.config.annotation.EnableWebMvc;
        import org.therapfoods.service.MenuService;
        import org.therapfoods.service.MenuServiceImpl;

        import java.util.Properties;
        import java.util.ResourceBundle;


@EnableWebMvc
@Configuration
@EnableTransactionManagement
@EnableAspectJAutoProxy(proxyTargetClass = true)
public class DatabaseConfig  {
    @Bean
    DriverManagerDataSource dataSource(){
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        ResourceBundle config = ResourceBundle.getBundle("db");
        String dbms = config.getString("dbms");
        String dbURl = config.getString("dbURL");
        String dbName = config.getString("dbName");
        String userName = config.getString("userName");
        String password = config.getString("password");
        String connectionString = "jdbc:" + dbms + ":" + dbURl + dbName;
        dataSource.setUsername(userName);
        dataSource.setPassword(password);
        dataSource.setUrl(connectionString);
        dataSource.setDriverClassName("com.mysql.jdbc.Driver");
        return dataSource;
    }

    @Bean
    PlatformTransactionManager TransactionManager(SessionFactory sessionFactory){
        HibernateTransactionManager txManager= new HibernateTransactionManager();
        txManager.setSessionFactory(sessionFactory);
        return txManager;
    }
    @Bean
    LocalSessionFactoryBean sessionFactory(DriverManagerDataSource dataSource){

        LocalSessionFactoryBean localSessionFactoryBean =  new LocalSessionFactoryBean();
        //localSessionFactoryBean.setPackagesToScan("com.forum.hibernateTestEntity");
        //localSessionFactoryBean.setAnnotatedClasses(FoodRegistry.class,Menu);
        localSessionFactoryBean.setPackagesToScan("org.therapfoods.entity");
        localSessionFactoryBean.setDataSource(dataSource);
        localSessionFactoryBean.setHibernateProperties(getHibernateProperties());
        return localSessionFactoryBean;
    }
    private Properties getHibernateProperties() {
        Properties properties = new Properties();
        properties.setProperty("hibernate.hbm2ddl.auto", "create");
        return properties;
    }

    @Bean
    public MenuService menuService()
    {
        return new MenuServiceImpl();
    }


}

