package org.therapfoods.utils;

public  enum MealConsumptionStatus { CONSUMED, NOT_CONSUMED }