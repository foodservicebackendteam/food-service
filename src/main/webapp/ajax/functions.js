
function getFoodMenu()
{
    jQuery.ajax({
            type: "GET",
            url: "http://localhost:8080/menus",
            success: function (data, status, jqXHR) {
                var foodList = "";

                for (i = 0; i < data.length; i++) {
                    foodList += (data[i].foodName) + " , ";
                }

                document.getElementById("foodMenu").innerHTML = foodList;
            },

            error: function (jqXHR, status) {
                alert("error");
            }
        }
    )
}