package org.therapfoods.controller;


import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;


import org.junit.Before;
import org.junit.Test;
import org.springframework.test.web.servlet.MockMvc;


/**
 * Created by minhazul.haq on 11/17/14.
 */


public class MenuControllerTest
{
    MockMvc mockMvc;

    MenuController menuController = new MenuController();

    @Before
    public void setup()
    {
        this.mockMvc = standaloneSetup(menuController).build();
    }

    //@Test
    public void testGetFoodItems() throws Exception
    {

        mockMvc.perform(get("/menus")).andDo(print()).andExpect(content().string("{\"foodName\":\"Biriyani\"}"));
    }
}